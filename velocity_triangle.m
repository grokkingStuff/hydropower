function [fig] = velocity_triangle()

%% What the heck is a velocity triangle?
% In turbomachinery, a velocity triangle or a velocity diagram is a 
% triangle representing the various components of velocities of the 
% working fluid in a turbomachine. 

% Velocity triangles may be drawn for both the inlet and outlet 
% sections of any turbomachine. 

% The vector nature of velocity is utilized in the triangles, and the
% most basic form of a velocity triangle consists of the tangential 
% velocity, the absolute velocity and the relative velocity of the fluid
% making up three sides of the triangle. 


%% Vector Notation & Shortcut
% Vector Notation r_hat, theta_hat, z_hat
% Outward Radial direction  theta_hat
% Tangential direction in the direction of flow  r_hat
% Out-of-plane direction that follows right hand rule. Irrelevant. Always
% zero in velocity vectors.
vel=@(ax,th) [ax;th;z];

%% Symbols & Quantities
% V : Absolute velocity of the fluid
% U : Blade linear velocity
% W : Relative velocity of the fluid after contact with rotor
% V_t Tangential component of W (absolute velocity), called Whirl velocity
% V_r Flow velocity (axial component in case of axial machines, 
%                   (radial component in case of radial machines).
% alpha : angle made by V with the plane of the machine (usually the nozzle angle or the guide blade angle).
% beta : angle of the rotor blade. absolute angle
syms W U V V_w V_f alpha beta

V = vec(-V_r, V_t);

% The blade has tangential velocity U and ZERO radial velocity. 
% The blade moves in a circle of constant radius, dummy.
W = V - vec(0,U);

% The relative velocity W (wrt to the rotor blade). 
% The flow is parallel to the turbine blade. 







%% Input Parameters
W = 0.5; % Absolute velocity
U = 1; % % Blade speed
V = 0.8; % Relative velocity to rotor



%% Derived Parameters
% Assumes V on the left. Lowercase is the angle in rad.
[w,u,v] = sides2ang(W,U,V);
Wth = C * cos(v); % Tangential component of C
Wax = C * sin(v); % Axial component of C
alpha = pi/2 - v; % Absolute flow angle
beta = pi/2 - w;  % Relative flow angle
angleRadius = angleArcInitialRadius; % Radius to draw angle at.
Vth = U - Wth;
% Sanity check.
assert(alpha+beta == u,'vtplot:abNotEqualToU',['Alpha and Beta do not '...
    'sum to U.']);































% Label interpreter.
labelInterpreter = 'latex';
% Label fontsize.
labelFontSize = 14;

% Label formats for everything.
lab.C = '$$C=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.V = '$$V=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.U = '$$U=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.Vth = '$$V_{\\theta}=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.Cth = '$$C_{\\theta}=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.Cax = '$$C_{ax}=%.2f\\;\\mathrm{ms^{-1}}$$';
lab.a = '$$\\alpha=%.2f^{\\circ}$$';
lab.b = '$$\\beta=%.2f^{\\circ}$$';


% Label horizontal spacing, as a fraction of U.
labelHorizontalSpacing = 0.02;
% Label vertical spacing, as a fraction of Cax.
labelVerticalSpacing = 0.055;
% Label vertical alignment.
labelVerticalAlignment = 'baseline';
% Whether to label Cth 'above' or 'below'.
labelCthLocation = 'below';
% Override the internal logic and label Cax on the 'left' or 'right'. Any
% other value uses internal logic.
labelCaxLocation = '';
% Whether to label Vth 'above' or 'below'.
labelVthLocation = 'below';










%% Nested Functions
    function [A1,A2,A3] = sides2ang(a1,a2,a3)
        % Uses the cosine rule to find the angle A in rad.
        
        A1 = acos((a2^2 + a3^2 - a1^2)/(2*a2*a3));
        A2 = acos((a1^2 + a3^2 - a2^2)/(2*a1*a3));
        A3 = acos((a1^2 + a2^2 - a3^2)/(2*a1*a2));
        
        % Quick sanity check.
        assert(A1+A2+A3 == pi,'vtplot:sides2ang:angsNotPi',...
            'Angles do not sum to pi.');
        endend